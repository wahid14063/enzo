# accounts/urls.py
from django.urls import path

from .views import SignUpView
from .views import HomeView
from .views import LogInView
#from .views import PasswordResetView
#from .views import index
#from .views import updateTask
#from .views import deleteTask
#from .views import views


urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('signup/', SignUpView.as_view(), name='signup'),
    #path('login/', LogInView.as_view(), name='login'),
    #path('password_reset/', LogInView.as_view(), name='password_reset'),
]
