from django.test import SimpleTestCase
from django.urls import resolve,reverse

from accounts.views import HomeView,SignUpView

class TestUrls(SimpleTestCase):

    def test_url_homeView(self):
        url = reverse('home')
        self.assertEquals(resolve(url).func.view_class, HomeView)


    def test_url_signup(self):
        url=reverse('signup')
        self.assertEquals(resolve(url).func.view_class, SignUpView) 