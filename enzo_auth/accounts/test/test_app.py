from django.apps import apps
from django.test import TestCase
from accounts.apps import AccountsConfig,TasksConfig




class AccountsConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(AccountsConfig.name, 'accounts')
        self.assertEqual(apps.get_app_config('accounts').name, 'accounts')
		
class TasksConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(TasksConfig.name, 'tasks')
        self.assertEqual(apps.get_app_config('tasks').name, 'tasks')
		
		
		
		
