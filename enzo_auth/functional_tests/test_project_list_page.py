from selenium import webdriver
from tasks.models import Task
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
import time

class TestProjectListPage(StaticLiveServerTestCase):
    
    def setUp(self):
        self.browser = webdriver.Chrome('functional_tests/chromedriver.exe')

    def tearDown(self):
        self.browser.close()


    def test_home_page_not_logged_in_label(self):
        self.browser.get(self.live_server_url)
        #time.sleep(20)

        #The user requests the page for the first time
        alert = self.browser.find_element_by_class_name('home-form')

        self.assertEquals(
            alert.find_element_by_tag_name('p').text,
            'You are not logged in'
        )
        
    #def test_log_in_error_properly_displayed(self):

    def test_home_page_login_link_redirects_to_login_page(self):
        self.browser.get(self.live_server_url)

        add_url = self.live_server_url + '/accounts/login/'
        self.browser.find_element_by_tag_name('a').click()
        self.assertEquals(
            self.browser.current_url,
            add_url
        )


    def test_login_page_login_label(self):
        self.browser.get(self.live_server_url + '/accounts/login/')

        alert = self.browser.find_element_by_tag_name('h2').text

        self.assertEquals(
            alert,
            'Log In'
        )

    def test_login_page_user_name_label(self):
        self.browser.get(self.live_server_url + '/accounts/login/')

        alert = self.browser.find_element_by_tag_name('p').text

        self.assertEquals(
            alert,
            'Username:'
        )
 

    def test_signup_page_sign_up_label(self):
        self.browser.get(self.live_server_url + '/accounts/signup/')

        alert = self.browser.find_element_by_tag_name('h2').text

        self.assertEquals(
            alert,
            'Sign up'
        )

    def test_signup_page_hello_label(self):
        self.browser.get(self.live_server_url + '/accounts/signup/')

        alert = self.browser.find_element_by_tag_name('a').text

        self.assertEquals(
            alert,
            'Hello'
        )

