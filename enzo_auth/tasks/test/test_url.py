from django.test import SimpleTestCase
from django.urls import resolve,reverse
from tasks.views import index,updateTask,deleteTask,reportGeneration

class TestUrls(SimpleTestCase):

    def test_url_first(self):
        url = reverse('list')
        self.assertEquals(resolve(url).func, index)
    
    def test_url_update_task(self):
        url = reverse('update_task',args=['test-slug'])
        self.assertEquals(resolve(url).func, updateTask)
    
    def test_url_deletetask(self):
        url = reverse('delete',args=['test-slug'])
        self.assertEquals(resolve(url).func, deleteTask)
    
    def test_url_reportgenration(self):
        url = reverse('report_generation')
        self.assertEquals(resolve(url).func, reportGeneration)   