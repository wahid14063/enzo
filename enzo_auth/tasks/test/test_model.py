
from django.test import SimpleTestCase
from tasks.models import Task

class Testmodel(SimpleTestCase):
    

    def test_model_title(self):
        task=Task()
        task.title="Assginment 1"
        task.complete=True    
        self.assertEqual(task.title,"Assginment 1")
    def test_whole_model(self):
        task=Task()
        task.title="coding"
        task.complete=False
        self.assertEqual(task.title,"coding")
        self.assertEqual(task.complete,False)